# @summary Defines a limit file containing one or more entries
#
# @param name
#   This will be combined with the sequence number to create the filename in
#   /etc/security/limits.d/
#   The name will have the form "${sequence}_${name}"
#
# @param sequence
#   Two digit number used to build the filename.
#
# @param header_comment
#   Array of strings which will be added to the header of the limit file on
#   creation.
#
# @param entries
#   Keys represent the user or group name for which the entries in the
#   associated  followed by various attributes which are to be set in the limits
#   file.
type Limits::FileDefinition = Struct[
  name           => Pattern[/^[a-z][a-z0-9_-]{,18}[a-z0-9]$/],
  sequence       => Pattern[/^[0-9]{2}$/],
  header_comment => Array[String],
  entries        => Hash[String, Array[Limits::Entry]],
]
