# @param item
#  Particular item to limit. This is a fixed list see `man -s 5 limits.conf` for
#  detail.
#
# @param type
#   Specifies whether this value corresponds to soft, hard, or both types. If
#   both is specified two lines will be created in the resulting limits file.
#
# @param value
#   String representation of the limit value. Note that not all values are valid
#   for all limit entries. See `man -s 5 limits.conf` for details.
#
# @param Optional[comment]
#   One or more lines which will be added before the particular entry line or
#   lines. If absent no comment lines are inserted.
#
# @summary Defines a limit entry for a specific domain
type Limits::Entry = Struct[
  item              => Limits::Item,
  type              => Enum['hard','soft','both'],
  value             => String,
  Optional[comment] => Array[String],
]
