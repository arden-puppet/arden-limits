# limits

[![pipeline status](https://gitlab.com/arden-puppet/arden-limits/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-limits/commits/master) [![Version](https://img.shields.io/puppetforge/v/arden/limits.svg)](https://forge.puppet.com/arden/limits) [![coverage report](https://gitlab.com/arden-puppet/arden-limits/badges/master/coverage.svg)](https://gitlab.com/arden-puppet/arden-limits/commits/master)

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with limits](#setup)
    * [What limits affects](#what-limits-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with limits](#beginning-with-limits)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module allows the creation of one or more limits.conf format files within `/etc/security/limits.d/` on the target node. These can be declared individually or via the class itself.

## Setup

### What limits affects

Simply creates limits entries as specified within the target directory.
### Setup Requirements

You'll need puppetlabs/stdlib.

### Beginning with limits

The following yaml config defines two limits entries:

```yaml
limits::array_limits:
  sequence: '10',
  header_comment:
    - 'Prevents errors when higher pipeline loads execute.'
    - 'See https://docs-snaplogic.atlassian.net/wiki/spaces/SD/pages/1438367/Installing+a+Snaplex+on+Linux'
  entries:
    snapuser:
      - item: nproc
        type: soft
        value: 8192
      - item: nproc
        type: hard
        value: 65536
      - item: nofile
        type: soft
        value: 8192
      - item: nofile
        type: hard
        value: 65536
  sap_base:
    sequence: '10'
    header_comment:
      - 'See the RHEL7 master note https://launchpad.support.sap.com/#/notes/2002167 for detail'
      - 'Note that this may handle Oracle requirements as well'
    entries:
      '@sapsys':
         - item: nproc
           type: soft
           value: unlimited
         - item: nofile
           type: both
           value: 65536
      '@sdba':
         - item: nofile
           type: both
           value: 32800
      '@dba':
         - item: nofile
           type: both
           value: 32800
```

The following puppet code declares the same limits entries detailed above directly.

```puppet
limits::file { 'snaplogic_limits':
  sequence       => '10',
  header_comment => [
    'Prevents errors when higher pipeline loads execute.',
    'See https://docs-snaplogic.atlassian.net/wiki/spaces/SD/pages/1438367/Installing+a+Snaplex+on+Linux',
  ],
  entries        => {
    'snapuser' => [
      {
        'item'  => 'nproc',
        'type'  => 'soft',
        'value' => '8192',
      },
      {
        'item'  => 'nproc',
        'type'  => 'hard',
        'value' => '65536',
      },
      {
        'item'  => 'nofile',
        'type'  => 'soft',
        'value' => '8192',
      },
      {
        'item'  => 'nofile',
        'type'  => 'hard',
        'value' => '65536',
      },
    ],
  },
}

limits::file { 'sap_base':
  sequence       => '10',
  header_comment => [
    'See the RHEL7 master note https://launchpad.support.sap.com/#/notes/2002167 for detail',
    'Note that this may handle Oracle requirements as well',
  ],
  entries        => {
    '@sapsys' => [
      {
        'item' => 'nproc',
        'type' => 'soft',
        'value' => 'unlimited',
      },
      {
        'item' => 'nofile',
        'type' => 'both',
        'value' => '65536',
      },
    ],
    '@sdba' => [
      {
        'item' => 'nofile',
        'type' => 'both',
        'value' => '32800',
      },
    ],
    '@dba' => [
      {
        'item' => 'nofile',
        'type' => 'both',
        'value' => '32800',
      },
    ],
  },
},

```

## Limitations

Note that while the 'items' are restricted to valid names no validation is
performed on their values.

## Development

Make a pull request and we'll figure it out!
