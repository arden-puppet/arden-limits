# @summary Defines a limit file containing one or more entries
#
# @param name
#   This will be combined with the sequence number to create the filename in
#   /etc/security/limits.d/
#   The name will have the form "${sequence}_${name}"
#
# @param sequence
#   Two digit number used to build the filename.
#
# @param header_comment
#   Array of strings which will be added to the header of the limit file on
#   creation.
#
# @param entries
#   Keys represent the user or group name for which the entries in the
#   associated  followed by various attributes which are to be set in the limits
#   file.
#
# @example Full definition
#   limits::file { 'snaplogic_limits':
#     sequence       => '10',
#     header_comment => [
#       'Prevents errors when higher pipeline loads execute.',
#       'See https://docs-snaplogic.atlassian.net/wiki/spaces/SD/pages/1438367/Installing+a+Snaplex+on+Linux',
#     ],
#     entries        => {
#       'snapuser' => [
#         {
#           'item'  => 'nproc',
#           'type'  => 'soft',
#           'value' => '8192',
#         },
#         {
#           'item'  => 'nproc',
#           'type'  => 'hard',
#           'value' => '65536',
#         },
#         {
#           'item'  => 'nofile',
#           'type'  => 'soft',
#           'value' => '8192',
#         },
#         {
#           'item'  => 'nofile',
#           'type'  => 'hard',
#           'value' => '65536',
#         },
#       ],
#     },
#   }
#
define limits::file (
  Pattern[/^[0-9]{2}$/] $sequence,
  Array[String] $header_comment,
  Hash[String, Array[Limits::Entry]] $entries,
  Stdlib::AbsolutePath $dir_limits = '/etc/security/limits.d',
) {
  unless($name =~ /^[a-z][a-z0-9_]{,18}[a-z0-9]$/) {
    $message = @("EOT"/L$)
      limits::file: '${name}' is not valid!
      |-EOT

    fail($message)
  }

  # TODO - add support for expansion based on pattern replacement
  $limit_arguments = {
    'header_comment' => $header_comment,
    'entries'        => $entries,
  }
  file { "${dir_limits}/${sequence}_${name}.conf":
    ensure  => 'file',
    mode    => '0644',
    content => epp('limits/conf.epp', $limit_arguments),
  }
}
