# @param dir_limits
#   Path to the limits.d configuration directory in which each limit file will
#   be created
#
# @param array_limits
#   Array of limits file entries. Each corresponds to a separate entity in
#   $dir_limits and will contain one or more entries. See the Limits::File type
#   definition for further details.
#
# @summary Generates an etc limits file for each entry in array_limits.
class limits (
  Stdlib::AbsolutePath $dir_limits  = '/etc/security/limits.d',
  Array[Limits::FileDefinition] $array_limits = [],
) {
  $array_limits.each |$file_entry| {
    $name = $file_entry['name']
    $sequence = $file_entry['sequence']
    $header_comment = $file_entry['header_comment']
    $entries = $file_entry['entries']

    # Generate the limits file
    limits::file { $name:
      sequence       => $sequence,
      header_comment => $header_comment,
      entries        => $entries,
    }
  }
}
