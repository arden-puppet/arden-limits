# Changelog

All notable changes to this project will be documented in this file.

## 0.1.0 (2021-06-09)

### Feature (3 changes)

- [feat: default path for file resources](arden-puppet/arden-limits@e05a9ece21f7c5bb8927640d8d23157543320c07)
- [feat: validate name strings and sequences](arden-puppet/arden-limits@4c5799d214f38319595fccbca845118053fe2853)
- [feat: limit files can now be declared individually](arden-puppet/arden-limits@434dc32b65364154245bb7d58bce4a51b9399402)

### Maintenance (4 changes)

- [maint: document release process and changelog](arden-puppet/arden-limits@a56dda259e63a2101bd25819af786cbfa1aa55c9)
- [maint: metadata updates & documentation](arden-puppet/arden-limits@4c69b1bb920ed75f25c3154b97efece2e357fddd)
- [maint: add code coverage tests](arden-puppet/arden-limits@c5096b8cf8b3d9fc1a040c6dfc1a1ad5c54b3de1)
- [doc: initial reference documentation](arden-puppet/arden-limits@40684a943daa01c6efa9aca26edd71fc5d241dfd)
