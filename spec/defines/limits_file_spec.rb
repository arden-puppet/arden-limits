# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

config_file_output = {
  'snaplogic' => {
    filename: '/etc/security/limits.d/15_snaplogic.conf',
    content: <<~HEREDOC,
      # === Managed by Puppet ===
      # Do not modify this file directly, changes will be overwritten!
      # See the `limits` class for detail!
      #
      # Prevents errors when higher pipeline loads execute.
      # See https://docs-snaplogic.atlassian.net/wiki/spaces/SD/pages/1438367/Installing+a+Snaplex+on+Linux
      snapuser    soft    nproc    8192
      snapuser    hard    nproc    65536
      snapuser    soft    nofile    8192
      snapuser    hard    nofile    65536
      HEREDOC
    parameters: {
      name: 'snaplogic',
      sequence: '15',
      header_comment: [
        'Prevents errors when higher pipeline loads execute.',
        'See https://docs-snaplogic.atlassian.net/wiki/spaces/SD/pages/1438367/Installing+a+Snaplex+on+Linux',
      ],
      entries: {
        'snapuser' => [
          {
            'item' => 'nproc',
            'type' => 'soft',
            'value' => '8192',
          },
          {
            'item' => 'nproc',
            'type' => 'hard',
            'value' => '65536',
          },
          {
            'item' => 'nofile',
            'type' => 'soft',
            'value' => '8192',
          },
          {
            'item' => 'nofile',
            'type' => 'hard',
            'value' => '65536',
          },
        ],
      },
    },
  },
  'sap_base' => {
    filename: '/etc/security/limits.d/10_sap_base.conf',
    content: <<~HEREDOC,
      # === Managed by Puppet ===
      # Do not modify this file directly, changes will be overwritten!
      # See the `limits` class for detail!
      #
      # See the RHEL7 master note https://launchpad.support.sap.com/#/notes/2002167 for detail
      # Note that this may handle Oracle requirements as well
      @sapsys    soft    nproc    unlimited
      @sapsys    hard    nofile    65536
      @sapsys    soft    nofile    65536
      @sdba    hard    nofile    32800
      @sdba    soft    nofile    32800
      @dba    hard    nofile    32800
      @dba    soft    nofile    32800
      HEREDOC
    parameters: {
      name: 'sap_base',
      sequence: '10',
      header_comment: [
        'See the RHEL7 master note https://launchpad.support.sap.com/#/notes/2002167 for detail',
        'Note that this may handle Oracle requirements as well',
      ],
      entries: {
        '@sapsys' => [
          {
            'item' => 'nproc',
            'type' => 'soft',
            'value' => 'unlimited',
          },
          {
            'item' => 'nofile',
            'type' => 'both',
            'value' => '65536',
          },
        ],
        '@sdba' => [
          {
            'item' => 'nofile',
            'type' => 'both',
            'value' => '32800',
          },
        ],
        '@dba' => [
          {
            'item' => 'nofile',
            'type' => 'both',
            'value' => '32800',
          },
        ],
      },
    },
  },
}

describe 'limits::file' do
  let(:params) do
    {
      dir_limits: '/etc/security/limits.d',
    }
  end

  context 'invalid title' do
    let(:title) { 'special$char*bonanza' }

    before(:each) do
      params.deep_merge(
        {
          name: title,
          sequence: '10',
          header_comment: [
            'See the RHEL7 master note https://launchpad.support.sap.com/#/notes/2002167 for detail',
            'Note that this may handle Oracle requirements as well',
          ],
          entries: {
            '@sapsys' => [
              {
                'item' => 'nproc',
                'type' => 'soft',
                'value' => 'unlimited',
              },
            ],
          },
        },
      )
    end

    it 'raises an error' do
      is_expected.to compile.and_raise_error(%r{limits::file: '#{Regexp.escape(title)}' is not valid!})
    end
  end

  config_file_output.each do |file, data|
    context "#{file} example" do
      let(:title) { file }

      before(:each) do
        params.deep_merge(
          data[:parameters],
        )
      end

      it 'compiles' do
        is_expected.to compile
      end

      it "creates #{data[:filename]} with the appropriate content" do
        is_expected.to contain_file(data[:filename]).with(
          ensure: 'file',
          mode: '0644',
          content: data[:content],
        )
      end
    end
  end
end
